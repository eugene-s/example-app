from collections import namedtuple
from typing import Type

ALLOWED_INSTRUCTIONS = (
    'start',
    'turn left',
    'turn right',
    'go distance x',
    'go distance x in n',
    'go distance x in s',
    'go distance x in e',
    'go distance x in w',
    'go until reach a landmark',
)

InstructionRoute = namedtuple('InstructionRoute', field_names=['instruction', 'blocks_count', 'landmark_name',
                                                               'coordinate_x', 'coordinate_y'],
                              defaults=['', None, '', None, None])


class BaseAdapter(object):
    def __init__(self):
        self.content = None

    def __iter__(self):
        return next(self)

    def __next__(self) -> InstructionRoute:
        """
        Iterate rows and transform to InstructionRoute
        :return: generator
        """
        raise NotImplementedError

    def load(self, stream) -> None:
        """
        Here's required to be content created
        :param stream: io stream
        """
        raise NotImplementedError


class HumanReadableAdapter(BaseAdapter):
    """
    Adapter to read human readable files. The format of files is `.txt`.
    todo: implement methods: load, next
    """


class JSONAdapter(BaseAdapter):
    """
    Adapter to read JSON files.
    """
    def __next__(self) -> InstructionRoute:
        for item in self.content:
            yield InstructionRoute(**item)

    def load(self, stream):
        import json
        self.content = json.load(stream)


class XMLAdapter(BaseAdapter):
    """
    Adapter to read XML files.
    todo: implement methods: load, next
    """


class CSVAdapter(BaseAdapter):
    """
    Adapter to read CSV files.
    todo: implement methods: load, next
    """


_TYPES_MAP: dict = {
    # 'human-readable': HumanReadableAdapter,
    'json': JSONAdapter,
    # 'xml': XMLAdapter,
    # 'csv': CSVAdapter,
}


def get_adapter_types():
    return _TYPES_MAP.keys()


def get_adapter_by_type(type_name: str) -> Type[BaseAdapter]:
    return _TYPES_MAP.get(type_name)

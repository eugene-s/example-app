from django import forms

from roboty.planner.adapters import get_adapter_types, get_adapter_by_type
from roboty.planner.backends import ProcessPlannerDataBackend


class PlannerUploadForm(forms.Form):
    """
    todo: add validation of file type. Add clean_method for type field.
    type field will be used to get the certain adapter to process raw data in backend
    """
    file = forms.FileField(label='Planner Data')
    type = forms.ChoiceField(choices=((t, t) for t in get_adapter_types()))

    def save(self):
        adapter = get_adapter_by_type(self.data['type'])
        backend = ProcessPlannerDataBackend(adapter)

        try:
            backend.run_import(self.files['file'])
        except Exception as exc:
            # todo process backend import exceptions
            raise

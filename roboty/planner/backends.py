import logging
from typing import Type

from django.db.transaction import atomic

from roboty.core.models import Route
from .adapters import BaseAdapter, ALLOWED_INSTRUCTIONS


logger = logging.getLogger(__name__)


class PlannerBackendError(Exception):
    pass


class ProcessPlannerDataBackend(object):
    def __init__(self, reader_cls: Type[BaseAdapter]):
        self.reader = reader_cls()

    @atomic
    def run_import(self, stream):
        self.reader.load(stream)

        prev_route = None
        new_route = Route()
        for index, action in enumerate(self.reader):
            if action.instruction not in ALLOWED_INSTRUCTIONS:
                logger.error('skip due to unknown instruction')
                raise PlannerBackendError(f'Unknown instruction {action.instruction}')

            if new_route and new_route.pk:
                prev_route = new_route
                new_route = Route()

            if 'start' == action.instruction:
                new_route.x = int(action.coordinate_x)
                new_route.y = int(action.coordinate_y)
                continue

            if 'turn' in action.instruction:
                turn_action = action.instruction.replace('turn', '').strip()
                new_route.dir = turn_action
                continue

            if 'go distance x in' in action.instruction:
                new_route.blocks_count = int(action.blocks_count)
                way = action.instruction.replace('go distance x in', '').strip()
                new_route.instruction = getattr(Route.const, f'INSTR_GO_{way.upper()}')
                new_route.save()
                if prev_route:
                    new_route.connect_above(prev_route)
                continue

            if 'go distance x' == action.instruction:
                new_route.instruction = Route.const.INSTR_GO
                new_route.blocks_count = int(action.blocks_count)
                new_route.save()
                if prev_route:
                    new_route.connect_above(prev_route)
                continue

            if 'go until reach a landmark' == action.instruction:
                new_route.instruction = Route.const.INSTR_UNTIL
                new_route.name = action.landmark_name
                new_route.save()
                if prev_route:
                    new_route.connect_above(prev_route)
                continue

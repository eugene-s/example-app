from django.urls import path
from . import views


urlpatterns = [
    path('', views.PlannerUploadView.as_view(), name='planner-upload'),
]

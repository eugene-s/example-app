from django.urls import reverse_lazy
from django.views.generic import FormView

from roboty.core.models import Route
from .forms import PlannerUploadForm


class PlannerUploadView(FormView):
    """
    todo: add get template and view to upload file.
    PlannerUploadForm will determine the type of file to use the certain adapter.
    """
    form_class = PlannerUploadForm
    template_name = 'planner-upload-view.html'
    success_url = reverse_lazy('planner-upload')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['start_routes'] = Route.objects.filter(prev=None)
        return context

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)


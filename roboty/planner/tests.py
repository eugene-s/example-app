import os

from django.test import TestCase

from roboty.core.models import Route
from roboty.planner.adapters import JSONAdapter
from roboty.planner.backends import ProcessPlannerDataBackend

path = os.path.dirname(__file__)


class JSONAdapterTestCase(TestCase):
    pass


class PlannerUploadFormTestCase(TestCase):
    pass


class PlannerUploadViewTestCase(TestCase):
    pass


class ProcessPlannerDataBackendTestCase(TestCase):
    def test_import(self):
        file_path = os.path.join(path, 'tests', 'data.json')

        with open(file_path) as file:
            adapter_cls = JSONAdapter
            backend = ProcessPlannerDataBackend(adapter_cls)
            backend.run_import(file)

        self.assertEqual(Route.objects.count(), 4)

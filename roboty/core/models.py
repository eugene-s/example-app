from django.db import models

from roboty.core.constants import RouteConstants


class Route(models.Model):
    const = RouteConstants

    x = models.PositiveIntegerField(null=True)
    y = models.PositiveIntegerField(null=True)

    instruction = models.CharField(choices=const.INSTR_CHOICES, blank=True, max_length=16)
    blocks_count = models.PositiveIntegerField(null=True)

    dir = models.CharField(choices=const.DIR_CHOICES, blank=True, max_length=8)

    name = models.CharField(max_length=64, blank=True)

    next = models.OneToOneField('self', models.CASCADE, related_name='prev', null=True)

    class Meta:
        db_table = 'roboty_routes'

    def __str__(self):
        msg = self.get_instruction_display().format(x=self.blocks_count,
                                                    name=f'"{self.name}"')
        if self.dir:
            msg = f'turn {self.dir} and {msg}'
        return msg.capitalize()

    def connect_above(self, route: 'Route'):
        route.next = self
        route.save(update_fields=['next'])

    def routes(self):
        return self.__class__.objects.raw(f'''
            WITH RECURSIVE roboty_commands AS (
                SELECT r.id, r.next_id, r.instruction, r.name, r.x, r.y, r.dir, r.blocks_count
                    FROM roboty_routes r where r.id = %(start_id)s
                UNION
                SELECT r.id, r.next_id, r.instruction, r.name, r.x, r.y, r.dir, r.blocks_count
                    FROM roboty_routes r INNER JOIN roboty_commands rr on r.id = rr.next_id
            ) SELECT * FROM roboty_commands rc;
        ''', {'start_id': self.id})

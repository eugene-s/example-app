class RouteConstants(object):
    INSTR_GO_N = 'go n'
    INSTR_GO_S = 'go s'
    INSTR_GO_E = 'go e'
    INSTR_GO_W = 'go w'

    INSTR_GO = 'go'
    INSTR_UNTIL = 'until reach'

    INSTR_CHOICES = (
        (INSTR_GO_N, 'Go distance {x} in direction North'),
        (INSTR_GO_S, 'Go distance {x} in direction South'),
        (INSTR_GO_E, 'Go distance {x} in direction East'),
        (INSTR_GO_W, 'Go distance {x} in direction West'),
        (INSTR_GO, 'Go distance {x}'),
        (INSTR_UNTIL, 'Go until you reach a landmark {name}'),
    )

    DIR_LEFT = 'left'
    DIR_RIGHT = 'right'

    DIR_CHOICES = (
        (DIR_LEFT, 'Left'),
        (DIR_RIGHT, 'Right'),
    )

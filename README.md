# Robot City Navigation System

## Run the project

```bash
docker-compose build
docker-compose up django
```

## Run tests

```bash
docker-compose build
docker-compose run django-tests
```

## Example to get routes

```sql
WITH RECURSIVE roboty_commands AS (
    (SELECT r.x, r.y, r.instruction, r.blocks_count, r.dir, r.name, r.next_id, r.id FROM roboty_routes r where id = <start id>)
    UNION
    (SELECT r.x, r.y, r.instruction, r.blocks_count, r.dir, r.name, r.next_id, r.id FROM roboty_routes r
    INNER JOIN roboty_commands rr on r.id = rr.next_id)
) SELECT rc.x, rc.y, rc.instruction, rc.blocks_count, rc.dir, rc.name FROM roboty_commands rc;
```

## Setup Development

### Requirements

- Pyenv
- Python 3.7

### Installation steps

1. Create virtualenv:

```bash
pyenv install 3.7.4 roboty 
```


2. Install libraries:

```bash
pyenv local roboty
pip install -r requirements.txt 
```

### Load initial data

```bash
python manage.py loaddata example
```

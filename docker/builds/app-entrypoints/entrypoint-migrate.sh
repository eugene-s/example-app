#!/bin/ash

# bash default parameters
set -o errexit  # make your script exit when a command fails
set -o pipefail # exit status of the last command that threw a non-zero exit code is returned
set -o nounset  # exit when your script tries to use undeclared variables

until PGPASSWORD=roboty psql -h "database" -U "roboty" -c '\q'; do
  >&2 echo "Postgres is unavailable - sleeping"
  sleep 3
done

python manage.py migrate --no-input

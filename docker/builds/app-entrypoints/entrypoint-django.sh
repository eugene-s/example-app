#!/bin/ash

# bash default parameters
set -o errexit  # make your script exit when a command fails
set -o pipefail # exit status of the last command that threw a non-zero exit code is returned
set -o nounset  # exit when your script tries to use undeclared variables

until PGPASSWORD=roboty psql -h "database" -U "roboty" -c '\q'; do
  >&2 echo "Postgres is unavailable - sleeping"
  sleep 3
done

gunicorn --timeout ${GUNICORN_TIMEOUT:-30} \
  --log-level ${GUNICORN_LOGLEVEL:-info} \
  --worker-class ${GUNICORN_WORKERCLASS:-gevent} "$@" \
  --chdir ${APP_DIR} roboty.wsgi \
  -b :8000 \
  -u www-data -g www-data \
  --log-file -
